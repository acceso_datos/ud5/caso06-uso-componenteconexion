package prueba;

import java.sql.Connection;
import java.sql.SQLException;

import es.cipfpbatoi.conexion.ConexionBD;

public class App 
{
    public static void main( String[] args ) throws SQLException
    {
    	// Usa valores de conexión por defecto
        Connection con = ConexionBD.getConexion();
		System.out.println(con);
		ConexionBD.cerrar();
		
		// Configura nuevos datos de conexión
		ConexionBD.setUser("batoi");
		ConexionBD.setPassword("1234");
		ConexionBD.setIp("192.168.56.101");
		ConexionBD.setJdbc("jdbc:postgresql");
		ConexionBD.setBd("batoi?currentSchema=empresa_ad");
		con = ConexionBD.getConexion();
		System.out.println(con);
		ConexionBD.cerrar();
    }
}
